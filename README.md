# YStruder: PCB Designs 

![](pcbs.jpg?raw=true)

The PCB designs were made in Kicad version 5.0.1.
The Ystruder design is based on two PCBs (ESP32 and Teensy) which are kept in separate folders.
Gerbers are also provided which can be sent directly to a PCB production service. 

Teensy footprint and library by Ricardo Band: https://github.com/XenGi/teensy_library 
